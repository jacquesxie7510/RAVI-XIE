<?php 
	//déclare la bdd 
	try
	{
		$bdd = new PDO('mysql:host=localhost;dbname=projet', 'root', '');
	}
	catch(Exception $e)
	{
	    die('Erreur : '.$e->getMessage());
	}
	
	// récupérer les info: nom et recette

	$mydrink = isset($_POST['nom_c']) ? $_POST['nom_c'] : NULL;
	$mydrink = $_POST['nom_c'];
	$homemade = isset($_POST['recette']) ? $_POST['recette'] : NULL;
	$homemade = $_POST['recette'];
	 
	if(empty($mydrink && $homemade)){
		echo 'vide!';
	}
	else{
		//transfert les données vers la base de donnée wamp
		$req = $bdd->prepare('INSERT INTO boire(titre, recette, photo) VALUES(:nom_c, :recette, :photo)');
		$req->execute(array(
			'nom_c' => $mydrink,
			'recette' => $homemade,
			'photo' => 'img/no_image.png'
		));
		
		header('Location: account.php');
		

	}	
 ?>

