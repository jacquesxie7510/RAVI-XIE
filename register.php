	<?php require 'inc/header.php'; ?>

	<header class="intro-header" style="background-image: url('img/cocktail3.jpeg')"> 

	<?php require 'inc/middle.php'; ?>

	<?php

	if(!empty($_POST)){

		$errors = array();
		require_once 'inc/bdd.php';

		if(empty($_POST['username'])){
			$errors['username']='Veuillez entrer un pseudo';
		}else{

			$req =$pdo->prepare('SELECT id FROM users WHERE username=?');
			$req->execute([$_POST['username']]);
			$user= $req->fetch();
			if($user){
				$errors['username']= 'ce pseudo est déjà pris';
			}

		}

		if(empty($_POST['email'])){
			$errors['email']='Veuillez entrer un pseudo';
		}else{

			$req =$pdo->prepare('SELECT id FROM users WHERE email=?');
			$req->execute([$_POST['email']]);
			$user= $req->fetch();
			if($user){
				$errors['email']= 'cet email est déjà pris';
			}

		}

		if (empty($_POST['password']) || ($_POST['password'] != $_POST['password_confirm'])) {

			$errors['password']= ' Le mot de passe et la confirmation de mot de passe doient être identique';
			
		}

		if(empty($errors)){

			
		$req= $pdo->prepare("INSERT INTO users SET username= ?,password= ?, email= ?");
		$password = password_hash($_POST['password'],PASSWORD_BCRYPT);
		 $req->execute([$_POST['username'],$password,$_POST['email']]); 
		 
		
			//header('Location: login.php');
			die('Vous êtes inscrit ! vous pouvez vous connecter!');
		
		}
		
		//debug($errors);
	}

				?>

	
	<h1>  inscription </h1>

<?php if(!empty($errors)): ?>
	<div class="alert alert-danger">
		<p> vous n'avez pas rempli correctement le formulaire </p>
		<ul>
		<?php foreach($errors as $error): ?>
		<li><?= $error; ?></li>
		<?php endforeach; ?>
	   </ul>

		</div>

		<?php endif; ?> 

	<form action= "" method="POST">

		<div class="form-group">

			<label for=""> Pseudo</label>

			<input type="text" name="username" class="form-control" required/>

		</div>

		<div class="form-group">

			<label for=""> Email</label>

			<input type="email" name="email" class="form-control" required/>
			
		</div>


		<div class="form-group">

			<label for=""> Mot de passe</label>

			<input type="password" name="password" class="form-control" required/>
			
		</div>


		<div class="form-group">

			<label for=""> Confirmez le mot de passe</label>

			<input type="password" name="password_confirm" class="form-control" required/>
			
		</div>

		<button type="submit" class="btn btn-primary"> M'inscrire</button>

	</form>

<?php require 'inc/footer.php'; ?>