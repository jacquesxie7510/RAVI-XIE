<?php require 'inc/header.php';?>
	
	<header class="intro-header" style="background-image: url('img/cocktail.jpg')"> 

	<?php require 'inc/middle.php'; ?>

<meta charset="utf-8">
<?php  
$bdd = new PDO('mysql:host=localhost;dbname=projet;charset=utf8', 'root', '');

$drink = $bdd->query('SELECT titre,recette FROM boire order by titre');

	if(isset($_GET['q']) AND !empty($_GET['q'])) {
		//éviter les faille de sécurite
		$q = htmlspecialchars($_GET['q']);
		
		$drink = $bdd->query('SELECT titre,recette FROM boire WHERE titre LIKE "%'.$q.'%" ORDER BY ID DESC');
		// si aucun résultat, on recherche dans le contenu
		if($drink->rowCount() == 0) {
			$drink = $bdd->query('SELECT titre,recette FROM boire WHERE CONCAT(titre, recette) LIKE "%'.$q.'%" ORDER BY ID DESC');
		}
	}
?>

<form method="GET">
	<input type="search" name="q"  placeholder="Recherche..."/>
	<input type="submit"  value="Valider"/>
</form>

<?php if($drink->rowCount() > 0){ ?>
	<ul>
	<!-- recherche dans le titre -->
	<?php while($data = $drink->fetch()) { ?>
		<li> <?= $data['titre'] ?> <?= $data['recette'] ?>  </li>
		
	<?php } ?>		
	</ul>
<?php } else { ?>
	Aucun résultat pour <?= $q ?>...
<?php } ?>

<?php require 'inc/footer.php'; ?>

